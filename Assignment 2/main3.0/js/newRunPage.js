// Code for the Measure Run page.
function pressForStartLocation(){
	//currentLocationArray.push();
    newrun=new Run()
	newrun.setcurrentLatitude=startLat;
	newrun.setcurrentLongitude=startLong;
    
    //START TIME
    var startTimeAndDate ="Date-"+day+"/"+month+" Time-"+hour+":"+minutes
    Run.setLocationDateAndTime=startTimeAndDate 
    let marker = new mapboxgl.Marker({
        color: "blue"
    });
    marker.setLngLat(startLat,startLong);
    marker.addTo(map);
    
} 



//RANDOM DISTANCE GENERATOR
function randomDestination(){
    //MAX AND MIN VALUES
    var maxRandomDistance=0.001;
    var minRandomDistance=-0.001;
    //RUN RANDOMIZER FORMULA FOR THE FIRST TIME
    var randomLatitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    var randomLongitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    //RERUNRANDOMIZER FORMULA IF ITS NOT WITHIN THE NEEDED VALUE
    while (randomLatitude >-0.0004 && randomLatitude < 0.0004){
        randomLatitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    }
    while (randomLongitude >-0.0004 && randomLongitude < 0.0004){
        randomLongitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    }
    var destinationLatitude=startLat+Number(randomLatitude);
    var destinationLongitude=startLong+Number(randomLongitude);
	newrun.setDestinationLatitude=destinationLatitude;
	newrun.setDestinationLongitude=destinationLongitude;

    
let marker = new mapboxgl.Marker({
    color: "blue"
});
marker.setLngLat(destinationLatitude,destinationLongitude);
marker.addTo(map);

     
    target = {
        latitude : range(destinationLatitude-0.00006666, destinationLatitude+0.00006666),
        longitude: range(destinationLongitude-0.00006666, destinationLongitude+0.00006666)
    };
    /*document.getElementById("demo2").innerHTML = `
    Random Latitude:${randomLatitude}<br> 
    Random Latitude:${randomLongitude} <br>
    destination Latitude:${newrun.getDestinationLocationLatitude}<br>
    destination Longitude:${newrun.getDestinationLocationLongitude}<br> 
     `*/
    
}

function success(pos) {
  if (target.latitude === pos.coords.latitude && target.longitude === pos.coords.longitude) {
    console.log('Congratulations, you reached the target');
    navigator.geolocation.clearWatch(id);
  }
}


