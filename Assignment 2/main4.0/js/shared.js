// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

//OTHER VARIABLE
var startLat ;
var startLong;
var startAccuracy;
var newrun;
var id;
var target;
//DATE AND TIME VARIABLE
var clock= new Date();
var clock= new Date();
var month=clock.getMonth();
var day=clock.getDate();
var minutes=clock.getMinutes();
var hour=clock.getHours();
var id
var target

//OBJECT//
class Run
{
    constructor()
    {
        this.currentLatitude=0;
        this.currentLongitude=0;
        this.destinationLocationLatitude=0;
        this.destinationLocationLongitude=0;
        this.trackingArrayLatitude=[];
        this.trackingArrayLongitude=[];
        this.locationDateAndTime=""
        this.destinationDateAndTime=""
    }
    //CURRENT POSITION USED TO GET START LOCATION
	get getcurrentLatitude()
    {
        return this.currentLatitude;
    }
    get getcurrentLongitude()
    {
        return this.currentLongitude;
    }
    set setcurrentLatitude(lat)
    {
        this.currentLatitude=lat;
    }
    set setcurrentLongitude(lon)
    {
        this.currentLongitude=lon;
    }
    //DESTINATION LOCATION GENERATED AFTER STARTLOCATION IS RECORDED
    get getDestinationLocationLatitude()
    {
        return this.destinationLocationLatitude;
    }
    get getDestinationLocationLongitude()
    {
    	return this.destinationLocationLongitude;
    }
    set setDestinationLatitude(deslat)
    {
        this.destinationLocationLatitude=deslat;
    }
    set setDestinationLongitude(deslon)
    {
        this.destinationLocationLongitude=deslon;
    }
    
    //TRACKING ARRAY 
    get getTrackingArrayLattitude()
    {
        return this.trackingArrayLatitude;
    }
    get getTrackingArrayLongitude()
    {
        return this.trackingArrayLongitude;
    }
    set setTrackingArrayLattitude(trackLatArray)
    {
        this.trackingArrayLatitude=trackLatArray;
    }
    set setTrackingArrayLongitude(trackLonArray)
    {
        this.trackingArrayLongitude=trackLonArray;
    }
    //DATE AND TIME FOR START LOCATION
    get getLocationDateAndTime()
    {
        return this.locationDateAndTime
    }
    set setLocationDateAndTime(lTAD)
    {
        this.locationDateAndTime=lTAD
    }
    //DATE AND TIME FOR DESTINATION LOCATION
    get getDestinationDateAndTime()
    {
        return this.destinationDateAndTime
    }
    set setDestinationDateAndTime(dTAD)
    {
        this.destinationDateAndTime=dTAD
    }
}

//OTHER FUNCTION
//RANGE FUNCTION
function range(start, end) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        ans.push(i);
    }
    return ans;
}


//GEOLOCATION GET POSITION
var options = {
  enableHighAccuracy: true,
  timeout: Infinity,
  maximumAge: 0
};
function startLocation(pos) {
	startLat =pos.coords.latitude;
	startLong=pos.coords.longitude;
	startAccuracy =pos.coords.accuracy;
	//console.log(startAccuracy)
}
function errorHandler(error){
    if(error.code == 0){
        alert("Unknown error");
    }
    if(error.code == 1){
        alert("Access denied by user");
    }
    if(error.code == 2){
        alert("Position unavailable");
    }
    if(error.code == 3){
        alert("Timed out");
    }
}
navigator.geolocation.getCurrentPosition(startLocation, errorHandler, options);

if(typeof(map) != "undefined"){        
    //ON MAP TRACKING
    map.addControl(new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    }));
}

//GEOLOCATION WATCH POSITION
function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
} 


//CLASS CREATION
    

function pressForStartLocation(){
	//currentLocationArray.push();
    newrun=new Run()
	newrun.setcurrentLatitude=startLat;
	newrun.setcurrentLongitude=startLong;
    
    //START TIME
    var startTimeAndDate ="Date-"+day+"/"+month+" Time-"+hour+":"+minutes
    Run.setLocationDateAndTime=startTimeAndDate
    /*document.getElementById("demo").innerHTML = `
    latitude:${newrun.getcurrentLatitude}<br> 
    longitude:${newrun.getcurrentLongitude} `*/
    
    
}


