// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

var startLat;
var startLong;
var startAccuracy;

//OBJECT//
class Run
{
    constructor(currentLati,currentLong/*,destinationLocatioArray*/)
    {
        this.currentLatitude=currentLati;
        this.currentLongitude=currentLong;
        //this.destinationLocationLatitude=trackingLocationArray[0];
        //this.destinationLocationLongitude=trackingLocationArray[1];
    }

	get getcurrentLatitude()
    {
        return this.currentLatitude;
    }
    get getcurrentLongitude()
    {
        return this.currentLongitude;
    }
    
    set setcurrentLatitude(lat)
    {
        this.currentLatitude=lat;
    }
    set setcurrentLongitude(lon)
    {
        this.currentLongitude=lon;
    }

    get getDestinationLocationLatitude()
    {
        return this.destinationLocationLatitude;
    }
    get getDestinationLocationLongitude()
    {
        return this.destinationLocationLongitude;
    }
    set setDestinationLatitude(deslat)
    {
        this.destinationLocationLatitude=deslat;
    }
    set setDestinationLongitude(deslon)
    {
        this.destinationLocationLongitude=deslon;
    }
}


var options = {
  enableHighAccuracy: true,
  timeout: Infinity,
  maximumAge: 0
};

function startLocation(pos) {
	startLat =pos.coords.latitude;
	startLong=pos.coords.longitude;
	startAccuracy =pos.coords.accuracy;
	
	pressForStartLocation();
	//console.log(startAccuracy)
}
	
function errorHandler(error)
    {
        if(error.code == 0){
           alert("Unknown error");
        }
        if(error.code == 1){
           alert("Access denied by user");
        }

        if(error.code == 2){
           alert("Position unavailable");
        }

        if(error.code == 3){
           alert("Timed out");
        }
    }

navigator.geolocation.getCurrentPosition(startLocation, errorHandler, options);

function pressForStartLocation(){
	//currentLocationArray.push();
	var newrun=new Run();
	newrun.setcurrentLatitude()=startLat;
	newrun.setcurrentLongitude()=startLong;
	//console.log(newrun.getcurrentLatitude);
	//console.log(newrun.getcurrentLongitude);
}  