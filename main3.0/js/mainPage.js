// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

/*function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}*/

var runListElement = document.getElementById('Run-list')

var runs=[]
savedRuns=JSON.parse(localStorage.getItem("RUNClassMemory"))
runs = savedRuns;
console.log(savedRuns);

        

var listHTML = "";
for(var i=0;i<runs.length;i++)
{
    listHTML += "<tr> <td onmousedown=\"listRowTapped("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + "Run : " +(i+1)+"</td></tr>";
    listHTML += '<span class="mdl-list__item-sub-title">RAN ON : '+runs[i].destinationDateAndTime+'</span>'
    //listHTML += "<div class=\"subtitle\">" + routes[i].airlineId + ", Stops: " + routes[i].stops +"</div></td></tr>";
}
// Insert the list view elements into the flights list.
runListElement.innerHTML = listHTML;

function listRowTapped(runnerIndex){
    //console.log(runs);
    //console.log(runs[runnerIndex]);
    //console.log(runs[runnerIndex].currentLongitude);

    
    //store runnerIndex in local storage
    localStorage.setItem("RunnerIndexMemory",runnerIndex)
     
    //change to viewRun
    location.href = 'viewRun.html'
    }
