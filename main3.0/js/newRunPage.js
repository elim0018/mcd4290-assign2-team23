// Code for the Measure Run page.
/*===============================
=           GEOLOCATION         =
===============================*/
//use for tracking and geting coordinates (Longitude and latitude)



//GEOLOCATION GET POSITION
var options = {
  enableHighAccuracy: true,
  timeout: Infinity,
  maximumAge: 0
};
function startLocation(pos) {
	startLat =pos.coords.latitude;
	startLong=pos.coords.longitude;
	startAccuracy =pos.coords.accuracy;
	//console.log(startLat)
    //console.log(startLong)
}
function errorHandler(error){
    if(error.code == 0){
        alert("Unknown error");
    }
    if(error.code == 1){
        alert("Access denied by user");
    }
    if(error.code == 2){
        alert("Position unavailable");
    }
    if(error.code == 3){
        alert("Timed out");
    }
}
navigator.geolocation.getCurrentPosition(startLocation, errorHandler, options);

//ON MAP TRACKING
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true
    },
    trackUserLocation: true
}));

//GEOLOCATION WATCH POSITION
function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
}
function success(pos)
{
	watchLatitude=pos.coords.latitude;
	watchLongitude=pos.coords.longitude;
}
id = navigator.geolocation.watchPosition(success, error, options);

/*=====================================
=   APPLICATION BUTTONS AND FUNCTIONS =
=====================================*/
function pressForStartLocation(){
    //create a new class ans storge current position
    newrun=new Run()
	newrun.setcurrentLatitude=startLat;
	newrun.setcurrentLongitude=startLong;
    
    //START TIME storage
    var startTimeAndDate ="Date-"+day+"/"+month+" Time-"+hour+":"+minutes
    newrun.setLocationDateAndTime=startTimeAndDate
    
    //PRINT MARKER ON MAP FOR START POINT
    let marker = new mapboxgl.Marker({
                color: "blue"
            });
            marker.setLngLat([newrun.getcurrentLongitude,newrun.getcurrentLatitude]);
            marker.addTo(map);
            
            let popup = new mapboxgl.Popup({
                offset: 45
            });
            
            popup.setText("Starting Marker");
            marker.setPopup(popup);

            map.easeTo({
                center: [newrun.getcurrentLongitude,newrun.getcurrentLatitude],
                zoom: 18
            });
}

//RANDOM DISTANCE GENERATOR
function randomDestination(){
    //GENERATE RANDOM POSITION
    //MAX AND MIN VALUES
    var maxRandomDistance=0.001;
    var minRandomDistance=-0.001;
    //RUN RANDOMIZER FORMULA FOR THE FIRST TIME
    var randomLatitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    var randomLongitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    //RERUNRANDOMIZER FORMULA IF ITS NOT WITHIN THE NEEDED VALUE
    while (randomLatitude >-0.0004 && randomLatitude < 0.0004){
        randomLatitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    }
    while (randomLongitude >-0.0004 && randomLongitude < 0.0004){
        randomLongitude=((Math.random() * (maxRandomDistance - minRandomDistance) 	+minRandomDistance).toFixed(10));
    }
    var destinationLatitude=startLat+Number(randomLatitude);
    var destinationLongitude=startLong+Number(randomLongitude);
	newrun.setDestinationLatitude=destinationLatitude;
	newrun.setDestinationLongitude=destinationLongitude;
    
    
    let destinationmarker = new mapboxgl.Marker({
                color: "red"
            });
            destinationmarker.setLngLat([newrun.getDestinationLocationLongitude,newrun.getDestinationLocationLatitude]);
            destinationmarker.addTo(map);
            
            let popup = new mapboxgl.Popup({
                offset: 45
            });
            
            popup.setText("Destination Marker");
            destinationmarker.setPopup(popup);

            map.easeTo({
                center: [newrun.getDestinationLocationLongitude,newrun.getDestinationLocationLatitude],
                zoom: 18
            })
    
    
    //distance from start to location
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }
    let lat2 = newrun.getDestinationLocationLatitude; 
    let lon2 = newrun.getDestinationLocationLongitude; 
    let lat1 = newrun.getcurrentLatitude; 
    let lon1 = newrun.getcurrentLongitude; 
    let R = 6371; // km 
    //has a problem with the .toRad() method below.
    let x1 = lat2-lat1;
    let dLat = x1.toRad();  
    let x2 = lon2-lon1;
    let dLon = x2.toRad();  
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * Math.sin(dLon/2) * Math.sin(dLon/2);  
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    let d = Math.round( (R * c * 1000) * 10 ) / 10;
    newrun.setDistanceFromStart=d
    document.getElementById("StartToDestination").innerHTML = `Start to Destination Distance : ${d}m<br>`
    trackAndFinishRun()
  
}

var record
var latitudeTrackArray  
var longitudeTrackArray


function trackAndFinishRun(){
	//get destination Location from Class
	var trackFinalDestinationLat=newrun.getDestinationLocationLatitude;
	var trackFinalDestinationLon=newrun.getDestinationLocationLongitude;
	//get array from class
	latitudeTrackArray=newrun.getTrackingArrayLatitude;
	longitudeTrackArray=newrun.getTrackingArrayLongitude;
	//start pushing Tracking latlng to array (every 10 second)
	var record=setInterval(() => {
		latitudeTrackArray.push(watchLatitude);
        console.log(latitudeTrackArray)
		longitudeTrackArray.push(watchLongitude);
        console.log(longitudeTrackArray)
        //CALCULATE DISTANCE BETWEEN CURRENT POSITION AND DESTINATION EVERY 10 SECOND
        let lat2 = newrun.getDestinationLocationLatitude; 
        let lon2 = newrun.getDestinationLocationLongitude; 
        let lat1 = watchLatitude; 
        let lon1 = watchLongitude; 
        let R = 6371; // km 
        //has a problem with the .toRad() method below.
        let x1 = lat2-lat1;
        let dLat = x1.toRad();  
        let x2 = lon2-lon1;
        let dLon = x2.toRad();  
        let a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * Math.sin(dLon/2) * Math.sin(dLon/2);  
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        let d = Math.round( (R * c * 1000) * 10 ) / 10;
        document.getElementById("TrackingToDestination").innerHTML = `Location to Destination Distance : ${d}m<br>`
		//IF DESTINATION IS REACHED adjust
		if ((watchLatitude>(trackFinalDestinationLat-0.00003333) &&
			 watchLatitude<(trackFinalDestinationLat+0.00003333)) &&
			(trackFinalDestinationLon>(trackFinalDestinationLon-0.00003333)&& 
			  trackFinalDestinationLon<(trackFinalDestinationLon+0.00003333)))
		{
			newrun.setTrackingArrayLatitude=latitudeTrackArray;
			newrun.setTrackingArrayLongitude=longitudeTrackArray;
			newrun.setDestinationDateAndTime="Date-"+day+"/"+month+" Time-"+ hour +":"+ minutes;
            clearInterval(record);
			navigator.geolocation.clearWatch(id);
            newrun.setCompleteStatus="COMPLETED"
            alert("Run Complete");
            stopAndSave();
			
			
		}
	},10000);
}

//STOP AND SAVE will activate once achievement is met in TRACK AND FINSIH 
function stopAndSave(){
    //CHECK ARRAY AND SAVE
	if (localStorage.getItem('RUNClassMemory')==null){
        savedRuns.push(newrun);
        localStorage.setItem('RUNClassMemory',JSON.stringify(savedRuns));
    }
    else{
        savedRuns=JSON.parse(localStorage.getItem('RUNClassMemory'));
        savedRuns.push(newrun);
        localStorage.setItem('RUNClassMemory',JSON.stringify(savedRuns));
    }
    alert("Run is Saved");
    window.history.back();
}



function stopRunButton(){
    newrun.setTrackingArrayLatitude=latitudeTrackArray;
    newrun.setTrackingArrayLongitude=longitudeTrackArray;
    newrun.setDestinationDateAndTime="Date-"+day+"/"+month+" Time-"+ hour +":"+ minutes;
    newrun.setCompleteStatus="FAILED"
    clearInterval(record);
    navigator.geolocation.clearWatch(id);
    //CHECK ARRAY AND SAVE
    if (localStorage.getItem('RUNClassMemory')==null){
        savedRuns.push(newrun);
        localStorage.setItem('RUNClassMemory',JSON.stringify(savedRuns));
    }
    else{
        savedRuns=JSON.parse(localStorage.getItem('RUNClassMemory'));
        savedRuns.push(newrun);
        localStorage.setItem('RUNClassMemory',JSON.stringify(savedRuns));
    }
   /* savedRuns.push(newrun)
    jsonRunClassArray=JSON.stringify(savedRuns);
    localStorage.setItem('RUNClassMemory',jsonRunClassArray)*/
	alert("Run is Saved");
    window.history.back();
    
}


