// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];


//OTHER VARIABLE
var startLat ;
var startLong;
var startAccuracy;
var newrun;
var id;
var target;
var jsonRunClassArray
var startToDestinationDistance
//DATE AND TIME VARIABLE
var clock= new Date();
var clock= new Date();
var month=clock.getMonth();
var day=clock.getDate();
var minutes=clock.getMinutes();
var hour=clock.getHours();
var id
var target

//OBJECT//
class Run
{
    constructor()
    {
        this.currentLatitude=0;
        this.currentLongitude=0;
        this.destinationLocationLatitude=0;
        this.destinationLocationLongitude=0;
        this.trackingArrayLatitude=[];
        this.trackingArrayLongitude=[];
        this.locationDateAndTime=""
        this.destinationDateAndTime=""
        this.complete=""
        this.startToDist=0
    }
    //CURRENT POSITION USED TO GET START LOCATION
	get getcurrentLatitude()
    {
        return this.currentLatitude;
    }
    get getcurrentLongitude()
    {
        return this.currentLongitude;
    }
    set setcurrentLatitude(lat)
    {
        this.currentLatitude=lat;
    }
    set setcurrentLongitude(lon)
    {
        this.currentLongitude=lon;
    }
    //DESTINATION LOCATION GENERATED AFTER STARTLOCATION IS RECORDED
    get getDestinationLocationLatitude()
    {
        return this.destinationLocationLatitude;
    }
    get getDestinationLocationLongitude()
    {
    	return this.destinationLocationLongitude;
    }
    set setDestinationLatitude(deslat)
    {
        this.destinationLocationLatitude=deslat;
    }
    set setDestinationLongitude(deslon)
    {
        this.destinationLocationLongitude=deslon;
    }
    //TRACKING ARRAY 
    get getTrackingArrayLatitude()
    {
        return this.trackingArrayLatitude;
    }
    get getTrackingArrayLongitude()
    {
        return this.trackingArrayLongitude;
    }
    set setTrackingArrayLatitude(trackLatArray)
    {
        this.trackingArrayLatitude=trackLatArray;
    }
    set setTrackingArrayLongitude(trackLonArray)
    {
        this.trackingArrayLongitude=trackLonArray;
    }
    //DATE AND TIME FOR START LOCATION
    get getLocationDateAndTime()
    {
        return this.locationDateAndTime
    }
    set setLocationDateAndTime(lTAD)
    {
        this.locationDateAndTime=lTAD
    }
    //DATE AND TIME FOR DESTINATION LOCATION
    get getDestinationDateAndTime()
    {
        return this.destinationDateAndTime
    }
    set setDestinationDateAndTime(dTAD)
    {
        this.destinationDateAndTime=dTAD
    }
    //COMPLETE STATUS
    get getCompleteStatus()
    {
        return this.complete
    }
    set setCompleteStatus(compl)
    {
        this.complete=compl
    }
    //DISTANCE FROM START
    get getDistanceFromStart()
    {
        return this.startToDist
    }
    set setDistanceFromStart(startTodis)
    {
        this.startToDist=startTodis
    }
}

//OTHER FUNCTION
//RANGE FUNCTION
/*function range(start, end) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        ans.push(i);
    }
    return ans;
}*/



//savedruns=localStorage.getItem('RUNClassMemory')
//console.log(savedruns)

