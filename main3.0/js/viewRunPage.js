var indexValue=localStorage.getItem("RunnerIndexMemory")
var listedRun=JSON.parse(localStorage.getItem("RUNClassMemory"))
var indexedListedRun = listedRun[indexValue]
console.log(indexedListedRun)

//START POSITION MARKER
let marker = new mapboxgl.Marker({
    color: "blue"
});
marker.setLngLat([indexedListedRun.currentLongitude,indexedListedRun.currentLatitude]);
marker.addTo(map);
let popup = new mapboxgl.Popup({
    offset: 45
});
popup.setText("Start Marker");
marker.setPopup(popup);

//DESTINATION MARKER
let endmarker = new mapboxgl.Marker({
    color: "red"
});
endmarker.setLngLat([indexedListedRun.destinationLocationLongitude,indexedListedRun.destinationLocationLatitude]);
endmarker.addTo(map);
let endpopup = new mapboxgl.Popup({
    offset: 45
});
endpopup.setText("Destination Marker");
endmarker.setPopup(endpopup);

map.easeTo({
    center: [indexedListedRun.currentLongitude,indexedListedRun.currentLatitude],
    zoom: 16
});

//PATHING
var pathingArray
var lengthcheck=indexedListedRun.trackingArrayLatitude;
pathingArray=[];
for(let i=0;i<lengthcheck.length;i++){
    var pushArr=[indexedListedRun.trackingArrayLongitude[i],indexedListedRun.trackingArrayLatitude[i]];
    pathingArray.push(pushArr);
};



//PRINT PATH ON MAP
map.on('load', function () {
 
    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates":pathingArray
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 8
        }
    });
});

//DELETE BUTTON TO DELETE FROM SAVE
function DeleteBtn(){  
	var tempRuns = []
	for (var i = 0; i < listedRun.length; i++) {
		if(indexValue != i)
			tempRuns.push(listedRun[i])
	};
    localStorage.setItem('RUNClassMemory',JSON.stringify(tempRuns));
    alert("Delete Success");
    window.history.back();
}

//INFORMATION PRINT OUT
document.getElementById("CompStats").innerHTML = `Completion Status : ${indexedListedRun.complete}<br>`
document.getElementById("stToDes").innerHTML = `Start To Destination Distance : ${indexedListedRun.startToDist}m<br>`